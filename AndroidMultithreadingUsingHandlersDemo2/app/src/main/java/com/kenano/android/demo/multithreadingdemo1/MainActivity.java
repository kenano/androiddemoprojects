package com.kenano.android.demo.multithreadingdemo1;

import android.Manifest;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.WorkerThread;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

/**
 * AdapterView.OnItemClickListener implements a click listener for the adapter bound to this
 * Activity.
 */
public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    public static final String TAG = MainActivity.class.getSimpleName();

    private ListView listview;
    private Button downloadImgButton;
    private String[] imagesArray;
    private LinearLayout progressBarSection;
    private Handler handler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        handler = new Handler();

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_CALENDAR) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        }

        imagesArray = getResources().getStringArray(R.array.urls);

        listview = (ListView) findViewById(R.id.images_lv);
        downloadImgButton = (Button) findViewById(R.id.download_image_btn);
        progressBarSection = (LinearLayout) findViewById(R.id.progressbar_section_ll);

        listview.setOnItemClickListener(this);
        downloadImgButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //create a background thread to download the image
                new Thread() {
                    @Override
                    public void run() {
                        //we want to include a process spinner to indicate download process.
                        //since this a UI process it cant be done on the background thread
                        //we want to bind the UI process to the MainActivity which is the current
                        //object. Normally we would use "this" to do this but since we are in
                        //an anon class, the context of "this" is lost (MainActivity  or the
                        //anon class we're in?). To clear this up use "MainActivity.this" to
                        //explicitly specify which class we mean.
//                        MainActivity.this.runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                progressBarSection.setVisibility(View.VISIBLE);
//                            }
//                        });

                        //The above used runOnUiThread() to sync background thread to UI thread.
                        //Below a handler is used for the same reason.
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                progressBarSection.setVisibility(View.VISIBLE);
                            }
                        });

                        downloadImageUsingThreads(imagesArray[0]);
                    }
                }.start();

            }
        });
    }



    //region network calls

    /**
     *
     *
     * 1. create a URL object to store url.
     * 2. open connection using url obj.
     * 3. read data using input stream into a byte array
     * 4. open file output to save image on sd card
     * 5. write data to output stream
     * 6. close connections
     *
     * @param url
     */
    public boolean downloadImageUsingThreads(String url) {
        //set this so false. Only change to true when successful.
        //this way only successes need to be set. If not set, failure implied.
        boolean successful = false;
        HttpURLConnection httpURLConnection = null;
        InputStream inputStream = null;
        int read;

        FileOutputStream fileOutputStream = null;
        File file;
        try {
            URL remoteUrl  = new URL(url);
            httpURLConnection =  (HttpURLConnection) remoteUrl.openConnection();
            inputStream = httpURLConnection.getInputStream();

            file = createImgFileInExtnStorage(url);
            fileOutputStream = new FileOutputStream(file);

            //this reads in one byte at a time. Very slow.
//            while ((read = inputStream.read()) >= 0) {
//                Log.d(TAG, "Read data: " + read);
//            }

            //a better way is to read many bytes at once using a buffer
            byte[] buffer = new byte[1024];
            while ((read = inputStream.read(buffer)) >= 0) {
                Log.d(TAG, "Number of bytes written into buffer: " + read);
                //copy all of the data in the buffer to the output stream.
                //(read is the number of bytes written to buffer.
                fileOutputStream.write(buffer, 0, read);
            }
            successful = true;
        } catch (MalformedURLException ex) {
            Log.w(TAG, "URL requested not formatted correctly." + ex);
        } catch (IOException ex) {
            Log.w(TAG, "IO exception downloading image.: " + ex);
        }
        finally {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException ex) {
                    Log.w(TAG, "IO Exception attempting to close inputstream: " + ex);
                }
            }
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException ex) {
                    Log.w(TAG, "IO Exception attempting to close outputstream");
                }
            }

            //this method is being ran in a background thread. To hide the procress bar we must
            //execute code on the Main thread.
            //In this example we will do the same but use handlers to accomplish. Commenting out
            //runOnUiThread
//            this.runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    progressBarSection.setVisibility(View.GONE);
//                }
//            });

            //use a handler to accomplish the above.
            handler.post(new Runnable() {
                @Override
                public void run() {
                    progressBarSection.setVisibility(View.GONE);
                }
            });
        }
        return successful;
    }

    private File createImgFileInExtnStorage(String url) {
        return new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES).getAbsolutePath() + "/" + Uri.parse(url).getLastPathSegment());
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Log.d(TAG, "In onItemClick Listener");
    }
    //endregion

}
