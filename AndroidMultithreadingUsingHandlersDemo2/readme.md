##part1
Create a httpurlconnection to get data from. This call is not called in its own thread so app will
crash due to the fact that code is called on the default (UI thread) thread.

This is also a good example of primitive URL calls.

Ive also created a basic ListView based UI.

##part2
Use FileInputStream and FileOutputStream to download file and save file locally. This will cause a 
crash since network operations must be done done in a background thread.

##part3
Use Java threads to run network download in a background thread.

##part4/5
Use runOnUiThread to show/hide progress bar. Since this involves the UI it must be invoked on the 
MainThread. Since we are in a background thread we must create another runnable to
execute through runOnUiThread(). 

There are issues with using Java Threads. First of all it is complicated using so many embedded
anon classes. 

If handset is rotated during download the UI is recreated. This causes the progress bar to become
invalid. It it bound to a specific instance of the UI.

##part6 
The previous tutorial used a Java thread to run the background process and runOnUiThread to update 
the UI. This part will use a handler to talk to the ui thread.

Since this example is some simple, the advantages from basic Java Threads are not obvious. Also
this approach still does not solve problems like what happens if user rotates handset in the middle
of a download.


